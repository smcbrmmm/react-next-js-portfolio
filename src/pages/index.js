import { useSession, signIn, signOut } from "next-auth/react";
import { useEffect, useMemo, useReducer, useRef, useState } from "react";
import Aos from "aos";
import "aos/dist/aos.css";

const reducer = (state, action) => {
  switch (action.type) {
    case "INCREMENT":
      return { count: state.count + 1, showText: state.showText };
    case "TOGGLE":
      return { count: state.count, showText: !state.showText };
    default:
      return state;
  }
};

export default function Home() {
  const { data: session } = useSession();
  const [active, setActive] = useState();
  const [datas, setDatas] = useState("");
  const [info, setInfo] = useState([1, 2, 3, 4]);
  const email = useRef();

  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, []);

  const calculate = (arr) => {
    let min = 0;
    for (let index = 0; index < arr.length; index++) {
      if (arr[index] > min) {
        min = arr[index];
      }
    }
    console.log("function was invoked");
    return min;
  };

  const memos = useMemo(() => calculate(info), [info]);

  const [state, dispatch] = useReducer(reducer, { count: 0, showText: true });

  function onSubmit(e) {
    e.preventDefault();

    console.log(!email.current.value ? "empty" : email.current.value);
  }

  if (!session) {
    return (
      <div className={"bg-blue-900 w-screen h-screen flex items-center"}>
        <div className="text-center w-full">
          <button
            onClick={() => signIn("google")}
            className={"bg-white p-2 px-4 rounded-lg"}
          >
            Login with google
          </button>
        </div>
      </div>
    );
  }

  return (
    <div>
      <section>
        <p>logged in {session.user.email}</p>
        <p>time out {session.expires}</p>

        <img src={session.user.image}></img>

        <div>{memos}</div>

        {state.count}
        {state.showText && <div> text has been shown.</div>}

        <form onSubmit={onSubmit}>
          <input
            ref={email}
            onChange={(event) => setDatas(() => event.target.value)}
            type="text"
          ></input>

          <div className="">
            <button
              onClick={() => {
                dispatch({ type: "INCREMENT" });
                dispatch({ type: "TOGGLE" });
              }}
              className={"bg-blue-500 text-white p-2 px-4 rounded-lg "}
            >
              Login
            </button>
          </div>
        </form>

        <button
          onClick={() => {
            setInfo(() => [...info, 6]);
            console.log(info);
          }}
          className={"bg-blue-500 text-white p-2 px-4 rounded-lg"}
        >
          Sign out
        </button>
      </section>

      <section>
        <div data-aos="zoom-in-up">section2</div>
      </section>
    </div>
  );
}
